# LS Assets Bundle

Simple bundle for managing assets within Symfony's development environment.

## Usage

Add the bundle to `AppKernel.php`.

```php
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
           ...
            new LS\AssetsBundle\LSAssetsBundle(),
            ...
        );
    }
}
```

Setup a config file inside `app/Resources/config/include.json` to define all the input/output files.

For example:

```json
{
  "lib": {
    "cwd": {
      "vendor": "app/Resources/vendor"
    },
    "files": [
      "%vendor%/jquery/dist/jquery.min.js",
      "%vendor%/underscore/underscore-min.js",
      "%vendor%/backbone/backbone-min.js"
    ]
  },
  "app": {
    "cwd": {
      "app": "app/Resources/public/js",
      "AppBundle": "src/AppBundle/Resources/public/js"
    },
    "files": [
      "%app%/App.js",
      "%app%/View/*.js",
      "%AppBundle%/*.js"
    ]
  }
}

```

Then inside your twig templates add the production and another other environment configurations.

```twig
{% if app.environment == 'prod' or app.environment == 'stage' %}
    <script src="{{ asset('js/lib.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
{% else %}
    {% for path in assets('lib') %}
    <script src="{{ asset('js/' ~ path) }}"></script>
    {% endfor %}
    {% for path in assets('app') %}
    <script src="{{ asset('js/' ~ path) }}"></script>
    {% endfor %}
{% endif %}
```

## Grunt Configuration

We need to symlink all the `cwd` directories we specified in our `include.json`.

We require (grunt-contrib-symlink)[https://github.com/gruntjs/grunt-contrib-symlink] and (grunt-contrib-uglify)[https://github.com/gruntjs/grunt-contrib-uglify] for this.
 
```js
module.exports = function (grunt) {
    var js = grunt.file.readJSON('app/Resources/config/include.json');
    
    grunt.initConfig({
        symlink: {
            options: {
                overwrite: false
            },
            expanded: {
                files: function () {
                    var symlinks = [];

                    // loop over each compiled file we intend to generate in prod
                    Object.keys(js).forEach(function (key) {
                        var file = js[key];

                        // now loop over each cwd to create a symlink for it
                        Object.keys(file.cwd).forEach(function (name) {
                            symlinks.push({
                                src: [file.cwd[name]],
                                dest: 'web/js/' + name
                            });
                        });
                    });

                    return symlinks;
                }()
            }
        },
        
        uglify: {
            dist: {
                files: function () {
                    var files = {};

                    Object.keys(js).forEach(function (output) {
                        var paths = [];

                        js[output].files.forEach(function (path) {
                            Object.keys(js[output].cwd).forEach(function (key) {
                                path = path.replace('%' + key + '%', js[output].cwd[key]);
                            });

                            paths.push(path);
                        });

                        files['dist/' + output] = paths;
                    });

                    return files;
                }()
            }
        }
    });
});
```