<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\AssetsBundle\Service;

class AssetService
{
    /**
     * @var string
     */
    protected $rootDir;

    /**
     * @param string $rootDir
     */
    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;;
    }

    /**
     * @param array $ref
     * @return array
     */
    public function getFiles($ref)
    {
        $output = [];
        $data = json_decode(file_get_contents(realpath($this->rootDir . '/Resources/config/include.json')), true);
        $paths = [];

        foreach ($data[$ref]['cwd'] as $key => $path) {
            $paths['%' . $key . '%'] = $this->rootDir . '/../' . $path;
        }

        foreach ($data[$ref]['files'] as $file) {
            $output = array_merge($output, str_replace(
                $paths,
                array_keys($data[$ref]['cwd']),
                $this->globRecursive(str_replace(array_keys($paths), $paths, $file), GLOB_NOCHECK)
            ));
        }

        return $output;
    }

    /**
     * @param string $pattern
     * @param int $flags
     * @return array
     */
    protected function globRecursive($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
            $files = array_merge($files, $this->globRecursive($dir . '/' . basename($pattern)));
        }
        return $files;
    }
}
