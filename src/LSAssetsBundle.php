<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\AssetsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LSAssetsBundle extends Bundle
{
}
