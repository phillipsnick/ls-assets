<?php
/**
 * @author  Nick Phillips <nick@linkstudios.co.uk>
 * @licence MIT
 */

namespace LS\AssetsBundle\Twig;

use LS\AssetsBundle\Service\AssetService;

class AssetExtension extends \Twig_Extension
{
    /**
     * @var AssetService
     */
    protected $assetService;

    /**
     * @param AssetService $assetService
     */
    public function __construct(AssetService $assetService)
    {
        $this->assetService = $assetService;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ls.assets.twig.asset';
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('assets', [$this, 'getAssets'])
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getAssets($ref)
    {
        return $this->assetService->getFiles($ref);
    }
}
